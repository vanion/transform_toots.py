#!/usr/bin/env python3
#
# This script takes toots from an mastodon archive and creates markdown templates for a Hugo blog.
# The resulting files need to be worked on, but the most reformatting will be done by this script
# It ignores boosts and replys and it doesnt detect followup toots.
#
# Todo:
# - Create an ignore list of toots not woth to save and copy only media of used tweets
# - Get rid of all the print debug commands and implement a debug mode
# - automatically copy the needed media files into the media_destination_path as an option
# - write some documentation, maybe find better file name :)

# external dependencies
try:
    import markdownify # https://pypi.org/project/markdownify/
except ModuleNotFoundError:
    print("External dependecy markdownify not found; HTML content will not be encoded to markdown")

# standard libraries
import json
import datetime
import pytz
import os
import re

# Paths to the files

mastodon_path = "./mastodon/"
json_file_path = mastodon_path + "/outbox.json"

markdown_path = "./markdown/"
media_destination_path = "/tootmedia/"


# Load the JSON file, that contains all ineractions
# TODO: try/except
with open(json_file_path, "r") as f:
    json_data = json.load(f)

# Load ignorelist.txt
# TODO: try/except
with open('ignorelist.txt', 'r') as f:
    ignorelist = set(f.read().splitlines())

# I only need the toots, which are in the orderedItems
json_data = json_data["orderedItems"]

# global variables
n = 0
file_list = ""

def dumpDebug(t,e):
    print(f"Unexpected toot encountered ({e})")
    print(json.dumps(t, indent=4))

# Iterate over all toots
for toot in json_data:
    # the toot istelf is in the object element

    # Getting basic information about the toot and decide if to process or not.
    try:

        t = toot["object"]

        # toot_id is unique in an instance   
        
        print (json.dumps(t["id"]))
        toot_id = t["id"].split("/")[-1]

        print(f"Processing toot: {toot_id} ... ", end='')

        # ignore boosts
        if toot["type"] == "Announce":
            print ("Ignoring Boost.")
            continue

        # print ("Vis: "+json.dumps(t["visibility"]))


        # ignore replys
        # it seems there are sometimes replys, who dont have the "inReplyTo" field.
        # Mabe when the related toot is deleted.
        if t["inReplyTo"] is not None :
            print (("Ignoring Reply."))
            continue

        if toot_id in ignorelist : 
            print ("Toot is on ignore list.")
            continue

        # Debugging foo
        # Daten für das md File sammeln.
        # print("ID: " + t["id"])
        # print("Date: " + t["published"])
        # print("Content: " + t["content"])
        # if t['attachment'] :
        #    print ("\nAttachment:")
        #    print (json.dumps(t['attachment']))
    except KeyError as e :
        dumpDebug(t,e)
        break
    
    # Count toots who are possible candidates for a blog article
    n += 1
    print (f"Useable Toot {n}.")

    # Getting Toot Timestamp (which is in UTC) 
    try: 
        toot_time_utc = datetime.datetime.strptime(t["published"], "%Y-%m-%dT%H:%M:%SZ")
    except (KeyError,TypeError,ValueError) as e :
        dumpDebug(e,t)
        break

    # Changing Timestamp to local Time (here: CET or CEST)
    toot_time_local = toot_time_utc.replace(tzinfo=pytz.utc).astimezone(
        pytz.timezone("Europe/Paris")
    )

    # building file_name where the toot will be written to.
    file_name = f"{toot_time_local.strftime('%Y-%m-%d')}_Toot_{toot_id}.md"

    # A Hugo article has a header and a body. the header contains meta data
    # Since this script is rather stupid it can't fill in the tags and categories for the
    # blog post. some manual labour will be neccessary
    file_header = f"""---
date: {toot_time_local.strftime("%Y-%m-%d %H:%M")}
draft: false
tags:
  - Toot
  - Musik
  - Vinyl
categories:
  - Mastodon
  - Musik
---
"""

    # Content is in html. Here it will be reformatted in markdown
    file_body = t["content"]
    try:
        file_body = markdownify.markdownify(file_body)
    except KeyError:
        continue # no content no cry
    except NameError:
        # ignore missing module markdownify; just copy html content
        pass

    # This regex replaces youtube links for a hugo short code
    # to shortcode
    pattern = r"<(https?:\/\/)?(www\.)?(youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9_-]+)>"
    replacement = r'{{< yt id="\4" >}}'
    file_body = re.sub(pattern, replacement, file_body)

    # Adding Attachment Information to the body
    try:
        attachments = t["attachment"]
    except KeyError:
        pass # silently ignore non-existent attachments
    else:
        # Iterating over all attachments - usually that's images.
        # Use the "name" tag as Description for the link.
        for a in t["attachment"]:
            # print("\n\nAttachment JSON:")
            # print(json.dumps(a))
            # Sometimes there is no description. Bad me.
            if not a["name"]:
                a["name"] = "No picture description."
            # Insert the Image in markdown.
            file_body += f"""
![{a["name"]}]({media_destination_path}{os.path.basename(a["url"])})
"""

            # Add used media to a copy script to easily copy
            # the files from the mastodon archive
            file_list += f"cp {mastodon_path} {a['url']} {markdown_path}{media_destination_path}\n"

    # Inserting Shortcode to the corresponding mastoron toot
    file_body += "\nOriginal Toot: " + '{{< mastodon status="' + t["id"] + '" >}}\n'
    
    

    # print ("\n\n")
    # print (file_header+file_body)

    # write out the markdown file
    with open(markdown_path + file_name, "w") as f:
        f.write(file_header + file_body)

# write out the Media copy script.
print("Saving Media copy script")
with open("file_copy_list", "w") as f:
    f.write(file_list)

# Done
